/*! \file  clockCH01S1.h
 *
 *  \brief Constants and function prototypes for clockCH01S1
 *
 *  \author jjmcd
 *  \date July 21, 2018, 4:10 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CLOCKCH01S1_H
#define	CLOCKCH01S1_H

#ifdef	__cplusplus
extern "C"
{
#endif


  void initSlaveOscillator(void);

#ifdef	__cplusplus
}
#endif

#endif	/* CLOCKCH01S1_H */

