/*! \file  clockCH01S1.c
 *
 *  \brief This file contains the mainline for clockCH01S1
 *
 *
 *  \author jjmcd
 *  \date July 21, 2018, 4:10 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include "clockCH01S1.h"

// Count for approx 1 second at 100 100 MHz
#define DELAY_COUNT 7307234L

void snore( long timer )
{
  long i;

  for ( i=0; i<timer; i++ )
    ;
}

/*! main - Mainline for clockCH01S1 */

/*!
 *
 */
int main(void)
{
  initSlaveOscillator();

  _TRISB5 = 0;

  while (1)
    {
      snore(DELAY_COUNT);
      _LATB5 ^= 1;
    }
  return 0;
}
