/*! \file  clockCH01.c
 *
 *  \brief This file contains the mainline for clockCH01
 *
 * Simple exercise to determine actual clock speed
 *
 *
 *  \author jjmcd
 *  \date July 21, 2018, 3:31 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <libpic30.h>
#include "clockCH01.h"
#include "clockCH01S1.h"

// Count for approx 1 second at 90 MHz
#define DELAY_COUNT 6576511L

void snore( long timer )
{
  long i;

  for ( i=0; i<timer; i++ )
    ;
}
/*! main - Mainline for clockCH01 */
/*!
 *
 */
int main(void)
{


  _TRISC8 = 0;  /* Blue LED */
  _TRISC9 = 0;  /* Pink LED */
  _LATC8 = 1;

  initOscillator();

  /* Program and start the slave */
//  _program_slave(1, 0, clockCH01S1);
//  _start_slave();

//  snore(DELAY_COUNT);
  _LATC8 = 0;

  while (1)
    {
      snore( DELAY_COUNT );
      _LATC9 ^= 1;
      snore( DELAY_COUNT );
      LATB ^= 0xffff;
    }

  return 0;
}
