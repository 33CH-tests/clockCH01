/*! \file  clockCH01.h
 *
 *  \brief Constants and function prototypes for clockCH01
 *
 *  \author jjmcd
 *  \date July 21, 2018, 3:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2018 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#ifndef CLOCKCH01_H
#define	CLOCKCH01_H

#ifdef	__cplusplus
extern "C"
{
#endif

  void initOscillator( void );


#ifdef	__cplusplus
}
#endif

#endif	/* CLOCKCH01_H */

